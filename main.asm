# Autores:
# Guilherme Andriotti Momesso, nº 8910441
# Gustavo Lima Lopes, nº 8910142
# Henrique Bonini de Britto Menezes, nº 8956690
# Luiza Villas Boas de Oliveira, nº 8503972
		.data
		.align	0

string:		.space	64
newline:	.asciiz	"\n"
init:		.asciiz	"Digite 0 para entrar com o número a ser convertido e 1 para sair"

		.text
		
# $s0: string em binário do número convertido
# $s1: usado para percorrer a string binária/inteiro em hexadecimal do número convertido
# $s2: string de entrada do usuário
# $s3: string após a virgula
# $s4: inteiro antes da virgula
# $s5: inteiro depois da virgula
# $s6: string do expoente
# $s7: string concatenada para cálculo da mantissa

main:		# Menu
		la	$a0, init
		li	$v0, 4
		syscall
		
		la	$a0, newline
		li	$v0, 4
		syscall
		
input_1:	li 	$v0, 5
		syscall
		
		beq 	$v0, 1, end
		beq 	$v0, $zero, continue
		j	erro_1


continue:	la	$s2, string

		# Lê a string entrada
		la	$a0, string
		li	$a1, 64
		li	$v0, 8
		syscall
		
		# Remove o '\n'
		move	$a0, $s2
		jal 	str_remnl

		# Aloca memória para a string
		li	$a0, 33
		li	$v0, 9
		syscall
		move	$s0, $v0
		move	$s1, $v0

		# Verifica o sinal
		move	$a0, $s2
		jal	get_sign
		beq	$v1, 2, erro_2
		sb	$v0, 0($s1)
		addi	$s1, $s1, 1
		move	$s2, $v1
		
		# Trata número
		move	$a0, $s2
		li	$a1, ','
		jal	str_trunct
		beq	$v1, 4, erro_4
		move	$t0, $v0
		move	$s3, $v1
		
		move	$a0, $t0
		jal	str_toint
		beq	$v1, 3, erro_3
		move	$s4, $v0
		
		move	$a0, $s3
		jal	str_toint
		beq	$v1, 3, erro_3
		move	$s5, $v0
		move	$t8, $v1
		
		# Testando se é o caso 0,0
		or	$t6, $s4, $s5
		beq	$t6, $zero, zero
		
		# Transformando de inteiro para string
		move	$a0, $s4
		jal	int_tostrbin
		move	$s4, $v0
		
		move	$a0, $s5
		move	$a1, $t8
		jal	dec_tostrbin
		move	$s5, $v0
				
		move	$a0, $s4
		jal	str_len
		move	$s6, $v0
		addi	$s6, $s6, 126
		
		
		move	$a0, $s4
		move	$a1, $s5
		jal	str_concat
		move	$s7, $v0
		move	$a0, $v0
		jal	count_until_1
		move	$t0, $v0		# número de dígitos até o primeiro 1
		mul	$t0, $t0, -1
		add	$s6, $s6, $t0		# sub $s6, $s6, $t0
		
		# Calcula o expoente
		move	$a0, $s6
		jal	int_tostrbin
		move	$a1, $v0
		li	$a0, 8
		jal	fill_zero
		move	$t0, $v0
		li	$t1, 8
expoente_loop:	beq	$t1, $zero, mantissa
		lb	$t2, 0($t0)
		sb	$t2, 0($s1)
		addi	$t0, $t0, 1
		addi	$s1, $s1, 1
		addi	$t1, $t1, -1
		j	expoente_loop
		
		# Calcula a mantissa
mantissa:	move	$a0, $s7
		jal	normalize
		move	$t0, $v0
		li	$t1, 23
mantissa_loop:	beq	$t1, $zero, mantissa_end
		lb	$t2, 0($t0)
		beq	$t2, $zero, mantissa_fill
		sb	$t2, 0($s1)
		addi	$t0, $t0, 1
		addi	$s1, $s1, 1
		addi	$t1, $t1, -1
		j	mantissa_loop
		
mantissa_fill:	beq	$t1, $zero, mantissa_end
		li	$t2, '0'
		sb	$t2, 0($s1)
		addi	$s1, $s1, 1
		addi	$t1, $t1, -1
		j	mantissa_fill
		
erro:		beq	$v1, 1, erro_1
		beq	$v1, 2, erro_2
		beq	$v1, 3, erro_3
		
erro_1:		jal	error_1

		la	$a0, newline
		li	$v0, 4
		syscall
		
		j	input_1
		
erro_2:		jal	error_2

		la	$a0, newline
		li	$v0, 4
		syscall
		
		j	continue
		
erro_3:		jal	error_3

		la	$a0, newline
		li	$v0, 4
		syscall
		
		j	continue
		
erro_4:		jal	error_4

		la	$a0, newline
		li	$v0, 4
		syscall
				
		j	continue
		
zero:		move	$a0, $s1
		jal	str_zero
		move	$s1, $v0
		
mantissa_end:	li	$t2, 0
		sb	$t2, 0($s1)			# Adiciona o '\0' ao fim da string
		
hex_string:	move 	$a0, $s0
		jal	str_hex
		move	$s1, $v0

main_end:	move	$a0, $s0
		li	$v0, 4
		syscall
		
		la	$a0, newline
		li	$v0, 4
		syscall
		
		move	$a0, $s1
		li	$v0, 34				# Imprime o valor no registrador em hexadecimal
		syscall
		
		la	$a0, newline
		li	$v0, 4
		syscall
		
		j	main
		
end:		li	$v0, 10
		syscall
