# Autores:
# Guilherme Andriotti Momesso, nº 8910441
# Gustavo Lima Lopes, nº 8910142
# Henrique Bonini de Britto Menezes, nº 8956690
# Luiza Villas Boas de Oliveira, nº 8503972

# Reúne sub-rotinas da conversão IEEE754
		.data
		.align	0
str:		.asciiz	"111"

		.text		
################################################################
# get_sign: Verifica o sinal (+/-) da string ###################	
# $a0: string
# $v0: '0': positivo / '1': negativo / 'x': inválido
# $v1: string a partir do sinal
# $t0: uso geral
		.globl	get_sign
get_sign:	lb	$t0, 0($a0)
		beq	$t0, '+', sign_pos
		beq	$t0, '-', sign_neg
		li	$v0, 'x'
		j	error

sign_pos:	li	$v0, '0'
		j	sign_ret

sign_neg:	li	$v0, '1'
		j	sign_ret

error:		li	$v1, 2
		jr	$ra

sign_ret:	move	$v1, $a0
		addi	$v1, $v1, 1
		jr	$ra

################################################################

################################################################
# count_until_1: Conta o número de casas até o dígito 1 ########	
# $a0: string
# $v0: número de casas
# $t0: usado para percorrer a string 1
# $t1: uso geral
		.globl	count_until_1
count_until_1:	move	$t0, $a0
		move	$v0, $zero

until_1_count:	lb	$t1, 0($t0)
		beq	$t1, $zero, until_1_jret
		beq	$t1, '1', until_1_jret
		addi	$t0, $t0, 1
		addi	$v0, $v0, 1
		j	until_1_count

until_1_jret:	jr	$ra

################################################################
# fill_zero: Preenche string até um número n de bits ###########
# $a0: número de bits
# $a1: string
# $v0: string preenchida
# $t0: usado para percorrer
# $t1: uso geral
# $t9: contador
		.globl	fill_zero
fill_zero:	addiu	$sp, $sp, 4
		sw	$a1, 0($sp)
		li	$v0, 9
		syscall
		move	$t3, $v0
		move	$t9, $zero

fill_count:	lb	$t1, 0($a1)
		beq	$t1, $zero, fill_1
		addi	$a1, $a1, 1
		addi	$t9, $t9, 1
		j	fill_count
		
fill_1:		sub	$t1, $a0, $t9

fill_loop1:	beq	$t1, $zero, fill_2
		li	$t2, 48
		sb	$t2, 0($t3)
		addi	$t1, $t1, -1
		addi	$t3, $t3, 1
		j	fill_loop1

fill_2:		lw	$a1, 0($sp)
		addiu	$sp, $sp, 4

fill_loop2:	lb	$t1, 0($a1)
		beq	$t1, $zero, fill_jret
		sb	$t1, 0($t3)
		addi	$a1, $a1, 1
		addi	$t3, $t3, 1
		j	fill_loop2

fill_jret:	jr	$ra
################################################################

################################################################
# normalize: Normaliza Conta o número de casas até o dígito 1 ########	
# $a0: string
# $v0: string normalizada
# $t0: usado para percorrer a string
# $t1: uso geral
		.globl	normalize
normalize:	move	$t0, $a0

normalize_loop:	lb	$t1, 0($t0)
		beq	$t1, '1', normalize_ret
		beq	$t1, $zero, normalize_jret
		addi	$t0, $t0, 1
		j	normalize_loop

normalize_ret:	addi	$t0, $t0, 1

normalize_jret:	move	$v0, $t0
		jr	$ra
