# Autores:
# Guilherme Andriotti Momesso, nº 8910441
# Gustavo Lima Lopes, nº 8910142
# Henrique Bonini de Britto Menezes, nº 8956690
# Luiza Villas Boas de Oliveira, nº 8503972
		.data
		.align 	0
msg_1:		.asciiz	"Opção incorreta, escolha 0 para converter ou 1 para sair do programa"
msg_2:		.asciiz	"Entrada inválida, o valor deve explicitar seu sinal"
msg_3:		.asciiz	"Valor inválido, digite um valor numérico"
msg_4:		.asciiz	"Formato incorreto de entrada, digite '<sinal>N,M'"

# Arquivo com rotinas de tratamento de erros

		.text
########################################################
# error_1: Opção incorreta escolhida ###################
		.globl	error_1
error_1:	la	$a0, msg_1
		li	$v0, 4
		syscall
		
		jr	$ra

########################################################
# error_2: Número digitado sem sinal ###################
		.globl	error_2
error_2:	la	$a0, msg_2
		li	$v0, 4
		syscall
		
		jr	$ra
		
########################################################
# error_3: Valor não numérico digitado #################
		.globl	error_3
error_3:	la	$a0, msg_3
		li	$v0, 4
		syscall
		
		jr	$ra

########################################################
# error_4: Valor não possui formato N,M ################
		.globl	error_4
error_4:	la	$a0, msg_4
		li	$v0, 4
		syscall
		
		jr	$ra
		