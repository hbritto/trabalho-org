# Autores:
# Guilherme Andriotti Momesso, nº 8910441
# Gustavo Lima Lopes, nº 8910142
# Henrique Bonini de Britto Menezes, nº 8956690
# Luiza Villas Boas de Oliveira, nº 8503972

# Reúne sub-rotinas de tratamento da parte fracionária do número

#######################################################################################
# dec_tostrbin: Converte a parte fracionária de um inteiro para uma string em binário #
# $a0: dígitos significativos
# $a1: número de casas após a vírgula
# $v0: string em binário
# $t2: string alocada
# $f0: 10.0
# $f1: valor em decimal resultante
# $f2: 2.0
# $f3: 1.0
# $t0: "0"
# $t1: "1"
# $t3: contador
# $t4: "\0"
		.data
float_10:	.float 10.0
float_2:	.float 2.0
float_1:	.float 1.0
float_0:	.float 0.0

		.text
		.globl dec_tostrbin
		
dec_tostrbin:	addi 	$sp, $sp, -8
		sw 	$a0, 4($sp)
		sw 	$ra, 0($sp)
		
		# Alocação da string
		#li	$a0, 24
		li	$a0, 129
		li	$v0, 9
		syscall
		move	$t2, $v0
		
		li	$t0, 48
		li	$t1, 49
		li	$t3, 0
		li	$t4, 0
		lw	$a0, 4($sp)

		# Inicialização dos valores float
		mtc1 	$a0, $f1
		cvt.s.w $f1, $f1
		
		l.s 	$f0, float_10
		l.s	$f2, float_2
		l.s	$f3, float_1
		l.s	$f4, float_0
		
divide:		beq 	$a1, $zero, divide_end
		div.s	$f1, $f1, $f0
		subi	$a1, $a1, 1

		j divide
		
divide_end:	
		
multiply:	# Float com valor: 0.0
		c.eq.s	$f1, $f4
		bc1t	multiply_end
		
		mul.s	$f1, $f1, $f2

		# Tamanho máximo da string
		#beq	$t3, 23, multiply_end
		beq	$t3, 128, multiply_end
		
		c.le.s	$f3, $f1
		bc1t 	maior_1
		bc1f	menor_1

maior_1:	sub.s	$f1, $f1, $f3
		sb	$t1, 0($t2)
		addi	$t2, $t2, 1
		addi	$t3, $t3, 1
		j	multiply

menor_1:	sb	$t0, 0($t2)
		addi 	$t2, $t2, 1
		addi	$t3, $t3, 1
		j	multiply
		
multiply_end:	# \0 na string
		sb	$t4, 0($t2)
		lw	$ra, 0($sp)
		addi	$sp, $sp, 8
		jr	$ra
#######################################################################################
