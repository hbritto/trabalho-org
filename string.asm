# Autores:
# Guilherme Andriotti Momesso, nº 8910441
# Gustavo Lima Lopes, nº 8910142
# Henrique Bonini de Britto Menezes, nº 8956690
# Luiza Villas Boas de Oliveira, nº 8503972

# Reune sub-rotinas de tratamento de string
		.text	
################################################################
# str_concat: Concatena duas strings ###########################	
# $a0: string 1
# $a1: string 2
# $v0: string concatenada
# $t0: usado para percorrer a string 1
# $t1: usado para percorrer a string 2
# $t2: uso geral
# $t3: usado para percorrer a string concatenada
# $t8: contador string 1
# $t9: contador string 2
		.globl	str_concat
		
str_concat:	move	$t0, $a0
		move	$t1, $a1
		move	$t8, $zero
		move	$t9, $zero

concat_count1:	lb	$t2, 0($t0)
		beq	$t2, $zero, concat_count2
		addi	$t0, $t0, 1
		addi	$t8, $t8, 1
		j	concat_count1
		
concat_count2:	lb	$t2, 0($t1)
		beq	$t2, $zero, concat_alloc
		addi	$t1, $t1, 1
		addi	$t9, $t9, 1
		j	concat_count2
		
concat_alloc:	move	$t0, $a0
		move	$t1, $a1
		move	$a0, $zero
		add	$a0, $t8, $t9
		addi	$a0, $a0, 1
		li	$v0, 9
		syscall
		move	$t3, $v0
		
concat_loop1:	beq	$t8, $zero, concat_loop2
		lb	$t2, 0($t0)
		sb	$t2, 0($t3)
		addi	$t0, $t0, 1
		addi	$t3, $t3, 1
		addi	$t8, $t8, -1
		j	concat_loop1
		
concat_loop2:	beq	$t9, $zero, concat_ret
		lb	$t2, 0($t1)
		sb	$t2, 0($t3)
		addi	$t1, $t1, 1
		addi	$t3, $t3, 1
		addi	$t9, $t9, -1
		j	concat_loop2
		
concat_ret:	la	$t2, 0($zero)
		sb	$t2, 0($t3)
		
concat_jret:	jr	$ra
################################################################

################################################################
# str_revers: Inverte uma string ###############################	
# $a0: string
# $v0: string invertida
# $t0: usado para percorrer a string
# $t1: usado para percorrer a string invertida
# $t2: uso geral
# $t9: contador string
		.globl	str_revers
		
str_revers:	move	$t0, $a0

		li	$a0, 24
		li	$v0, 9
		syscall
		move	$t1, $v0
		
		li	$t9, 0		# Contador: c
				
revers_count:	lb	$t2, 0($t0)		
		beq	$t2, $zero, revers_adj
		addi	$t9, $t9, 1	# c++	
		addi	$t0, $t0, 1
		j	revers_count	
		
revers_adj:	addi	$t0, $t0, -1

revers_loop:	beq	$t9, $zero, revers_ret
		lb	$t2, 0($t0)
		sb	$t2, 0($t1)
		
		addi	$t0, $t0, -1
		addi	$t1, $t1, 1
		addi	$t9, $t9, -1
		j	revers_loop
		
revers_ret:	la	$t2, 0($zero)
		sb	$t2, 0($t1)
		
revers_jret:	jr $ra
################################################################

################################################################
# str_trunct: Divide uma string ################################	
# $a0: string
# $a1: caracter de divisão
# $v0: string dividida
# $v1: string a partir da divisão
# $t0: usado para percorrer a string
# $t1: uso geral
# $t2: usado para percorrer a string dividida
# $t9: contador string
		.globl	str_trunct
		
str_trunct:	move	$t0, $a0
		move	$t9, $zero

trunct_count:	lb	$t1, 0($t0)
		beq	$t1, $a1, trunct_alloc
		beq	$t1, $zero, error_4
		addi	$t0, $t0, 1
		addi	$t9, $t9, 1
		j	trunct_count
		
trunct_alloc:	addi	$t0, $t0, 1
		move	$t0, $a0
		move	$a0, $t9
		addi	$a0, $a0, 1
		li	$v0, 9
		syscall
		move	$t2, $v0
		
trunct_loop:	beq	$t9, $zero, trunct_ret
		lb	$t1, 0($t0)
		sb	$t1, 0($t2)
		addi	$t0, $t0, 1
		addi	$t2, $t2, 1
		addi	$t9, $t9, -1
		j	trunct_loop
		
trunct_ret:	la	$t1, 0($zero)
		sb	$t1, 0($t2)

		move	$v1, $t0
		lb	$t1, 0($t0)
		beq	$t1, $zero, trunct_jret
		addi	$v1, $v1, 1
		j	trunct_jret
		
error_4:	li	$v1, 4
		
trunct_jret:	jr 	$ra
################################################################

################################################################
# str_toint: Converte uma string em um inteiro #################	
# $a0: string
# $v0: inteiro obtido
# $v1: número de caracteres da string
# $t0: usado para percorrer a string
# $t1: uso geral
# $t2: potência de 10
# $t3: inteiro usado nos cálculos
# $t9: contador string
		.globl	str_toint
		
str_toint:	move	$t0, $a0			# Passa o endereço da string para $t0
		move	$t9, $zero			# Inicia o contador em zero
# Conta o número de caracteres da string		
toint_count:	lb	$t1, 0($t0)
		beq	$t1, $zero, toint_pot
		addi	$t0, $t0, 1
		addi	$t9, $t9, 1
		j	toint_count

toint_pot:	addi	$t0, $t0, -1			# Volta uma posição na string, já que a atual aponta para o null-terminator
		li	$t2, 1				# Inicia a variável de potência 10
		li	$v0, 0				# Zera o valor para armazenar o inteiro convertido
		move	$v1, $t9			# Move o contador da string para o valor de retorno
# Converte a string para inteiro, realizando multiplicações pela potência de 10 da posição correspondente
toint_loop:	beq	$t9, $zero, toint_jret
		lb	$t1, 0($t0)
		addi	$t1, $t1, -48
		blt	$t1, $zero, error_3
		bgt	$t1, 9, error_3
		mul	$t3, $t1, $t2
		add	$v0, $v0, $t3
		
		addi	$t0, $t0, -1
		mul	$t2, $t2, 10			# Incrementando a potência de 10
		addi	$t9, $t9, -1			# Decementa o contador
		j	toint_loop

error_3:		li	$v1, 3
		
toint_jret:	jr	$ra
################################################################

################################################################
# str_toint: Converte uma string em um inteiro #################	
# $a0: string
# $v0: tamanho da string
# $t0: usado para percorrer a string
# $t1: uso geral
		.globl	str_len
str_len:	move	$v0, $zero
		move	$t0, $a0
		
len_loop:	lb	$t1, 0($t0)
		beq	$t1, $zero, len_jret
		addi	$t0, $t0, 1
		addi	$v0, $v0, 1
		j	len_loop
		
len_jret:	jr	$ra
################################################################

################################################################
# str_remnl: Remove o caracter '\n' da string ##################	
# $a0: string
# $t1: uso geral
		.globl	str_remnl
str_remnl:	
remnl_loop:	lb	$t0, 0($a0)
		beq	$t0, '\n', remnl_ret
		beq	$t0, $zero, remnl_jret
		addi	$a0, $a0, 1
		j	remnl_loop

remnl_ret:	li	$t0, 0
		sb	$t0, 0($a0)
		
remnl_jret:	jr	$ra
################################################################

################################################################
#str_hex: cria a string de hexadecimais ########################
# $a0: string em binário
# $t0: 2
# $t1: anda pela string
# $t2: 2^(0 -> 31)
# $t9: contador
# $v0: inteiro representado pela string
		.globl	str_hex
str_hex:	addi	$sp, $sp, -4
		sw	$ra, 0($sp)
		
		add	$t9, $zero, $zero		# Inicializando registradores
		addi	$t2, $zero, 1
		addi	$t0, $zero, 2
		add	$v0, $zero, $zero
		lb	$t1, 0($a0)
		
str_end_loop:	# Vai até o fim da string
		beq	$t1, 0, str_end_end
		addi	$a0, $a0, 1
		lb	$t1, 0($a0)
		addi	$t9, $t9, 1
		j	str_end_loop

str_end_end:	# Chegou ao fim da string
		subi	$a0, $a0, 1
		subi	$t9, $t9, 1

str_hex_loop:	beq	$t9, $zero, str_hex_end
		lb	$t1, 0($a0)
		subi	$t1, $t1, 48
		mul	$t1, $t1, $t2
		add	$v0, $v0, $t1
		mul	$t2, $t2, $t0
		subi	$a0, $a0, 1
		subi	$t9, $t9, 1
		j	str_hex_loop
		
str_hex_end:	lw	$ra, 0($sp)
		addi	$sp, $sp, 4
		jr	$ra
#################################################################################

#################################################################################
# str_zero: Trata o caso em que o número digitado é 0,0 #########################
# $a0: string a ser modificada
# $v0: endereço final da string
# $t0: '0'
# $t9: contador (inicia em 1 pois a primeira posição da string já foi preenchida
#	com o bit de sinal)
		.globl str_zero
str_zero:	addi	$sp, $sp, -4
		sw	$ra, 0($sp)
		
		addi	$t0, $zero, 48
		addi	$t9, $zero, 1

str_zero_loop:	beq	$t9, 30, str_zero_end
		sb	$t0, 0($a0)
		addi	$a0, $a0, 1
		addi	$t9, $t9, 1
		j	str_zero_loop

str_zero_end:	add	$v0, $zero, $a0
		lw	$ra, 0($sp)
		addi	$sp, $sp, 4
		jr	$ra
