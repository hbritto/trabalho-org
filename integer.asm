# Autores:
# Guilherme Andriotti Momesso, nº 8910441
# Gustavo Lima Lopes, nº 8910142
# Henrique Bonini de Britto Menezes, nº 8956690
# Luiza Villas Boas de Oliveira, nº 8503972

# Reúne sub-rotinas de tratamento de inteiros
		.text	
################################################################
# int_tostrbin: Converte um inteiro para uma string em binário #	
# $a0: inteiro
# $v0: string em binário
# $t0: usado como divisor na conversão
# $t1: uso geral
# $t2: usado para percorrer a string
# $t3: resto das divisões
		.globl	int_tostrbin
		
int_tostrbin:	addiu	$sp, $sp, -4
		sw	$ra, 0($sp)

		li	$t0, 2
		move	$t1, $a0
	
		li	$a0, 24				# Aloca uma string de 23 bytes(1 para cada algarismo binário)
		li	$v0, 9
		syscall
		move	$t2, $v0			# $s2 = endereço da string

tostrbin_loop:	div	$t1, $t0			# int/2
		mflo	$t1				# int = int/2
		mfhi	$t3				# $t3 = int%2
		
		addi	$t3, $t3, 48
		sb	$t3, 0($t2)			# Cada bit ocupará um byte da string
		addi	$t2, $t2, 1			# Avançando 1 posição na string
				
		beq	$t1, $zero, tostrbin_ret
		j	tostrbin_loop

tostrbin_ret:	move	$a0, $v0
		jal	str_revers
		
		lw	$ra, 0($sp)
		addiu	$sp, $sp, 4

tostrbin_jret:	jr	$ra
################################################################
